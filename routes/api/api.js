let superUser = require("../../SuperUser/su");

// Verify SuperUser && Admin

exports.apiVerify = function async (req, res) {
  
    let usernameAdmin = req.body.usernameAdmin;
    let password = req.body.password;
    let verifyUsernameAdmin = superUser.find(s => s.usernameAdmin === usernameAdmin)
    let verifyPassword = verifyUsernameAdmin.password === password;
  
    try {
    if(verifyUsernameAdmin && verifyPassword){

      req.session.usernameAdmin = verifyUsernameAdmin;
      res.redirect('/home');
    } else {
      next()
    };
    } catch (error) {
      res.render('index',{
        error: error
      })
    }
};