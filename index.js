const express = require("express");

const sessionHandler = require("express-session");
const cookieParser = require("cookie-parser");
const oneDay = 1000 * 60 * 60 * 24;

const bodyParser = require("body-parser");
const jsonParser = bodyParser.json();
const urlEncoded = bodyParser.urlencoded({ extended: false });

const { user_game, user_game_biodatum, user_game_history } = require('./models')

const app = express();

app.use(sessionHandler({
    secret: "secret",
    saveUninitialized:true,
    cookie: { maxAge: oneDay },
    resave: false
}));

app.use(cookieParser());

app.set('view engine', 'ejs');



const dotenv = require("dotenv");

dotenv.config();

const port = process.env.APP_PORT || 3000;

//View SuperUser

app.get('/', (req, res) => {
    res.render('index', { title: 'Binar Challenge FW C6' });
});
//API superuser
let { apiVerify } = require("./routes/api/api");

app.post("/api/verify", urlEncoded, apiVerify);

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=

//Views GROUP
app.get("/home", (req, res) => {
    let usernameAdmin = req.session.usernameAdmin;
    if (req.session.usernameAdmin){
       return user_game.findAll({
            where: {},
            order: [
                ['updatedAt', 'desc'],
                ['createdAt', 'desc']
            ], include: user_game_biodatum, include: user_game_history
        },usernameAdmin)
        .then(user_games => {
            res.render('SuperUser_Page/home', {
                user_games: user_games
            }
            );
        });

        
    } else {
        return res.status(401).render('unauthorized')
    }
    
  });

//View Show User
app.get("/home/show/:id", (req, res) => {
    if (req.session.usernameAdmin){
        user_game.findByPk(req.params.id)
        .then(userGame => {
            res.render('SuperUser_Page/show', {
            userGame: userGame,
            });
        });
         
     } else {
         return res.status(401).render('unauthorized')
     }
});


//View Create User
app.get("/home/create_user_game", (req, res) => {
    if (req.session.usernameAdmin){
        return res.render('SuperUser_Page/user_game/create')

        
    } else {
        return res.status(401).render('unauthorized')
    }
});

//View User Biodata
app.get("/home/show/:id/biodata", (req, res) => {
    if (req.session.usernameAdmin){
        let biodataId = req.params.id
        user_game_biodatum.findAll({
            where: {userId: req.params.id}
        })
        .then(Biodata => {
            res.render('SuperUser_Page/user_game_biodatum/show', {
            Biodata: Biodata,
            BiodataId: biodataId
            });
        });
         
     } else {
         return res.status(401).render('unauthorized')
     }
});

//View Edit User Game
app.get("/home/edit_user_game/:id", (req, res) => {
    if (req.session.usernameAdmin){
        user_game.findByPk(req.params.id)
        .then(userGame => {
            res.render('SuperUser_Page/user_game/edit', {
                userGame: userGame
            });
        });

        
    } else {
        return res.status(401).render('unauthorized')
    }
});

// View Edit  User Biodata
app.get("/home/show/:id/biodata/edit_biodata", (req, res) => {
    if (req.session.usernameAdmin){
            let BiodataId = req.params.id;
            user_game_biodatum.findAll({
            where: {userId: req.params.id}
        })
        .then(userGameB => {
            res.render('SuperUser_Page/user_game_biodatum/edit', {
            userGameB: userGameB,
            BiodataId: BiodataId
            });
        });
         
     } else {
         return res.status(401).render('unauthorized')
     }
});

//View Create User Biodata
app.get("/home/show/:id/biodata/create_biodata", (req, res) => {
    if (req.session.usernameAdmin){
            let BiodataId = req.params.id;
            user_game_biodatum.findAll({
            where: {userId: req.params.id}
        })
        .then(userGameB => {
            res.render('SuperUser_Page/user_game_biodatum/edit', {
            userGameB: userGameB,
            BiodataId: BiodataId
            });
        });
         
     } else {
         return res.status(401).render('unauthorized')
     }
});
   
//API GROUP=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-==-=-==-=-=-=-=-=-=-==-=-=-=-=-=-=

// API for create user game
app.post("/home/api/create_user_game", urlEncoded, async (req, res) => {
    try {
        let userGame = await user_game.create({
            username: req.body.username,
            email: req.body.email,
            password: req.body.password
        });

        res.redirect('/home');
    } catch (error) {
        res.render('SuperUser_Page/user_game/create', {
            error : error
        })
    }
});

//API for edit user game
app.post("/home/api/edit_user_game", urlEncoded, async (req, res) => {
    try {
        let userGame = await user_game.findByPk(req.body.id);
        userGame.username = req.body.username ? req.body.username : userGame.username;
        userGame.email = req.body.email ? req.body.email : userGame.email;
        userGame.password = req.body.password ? req.body.password : userGame.password;
        await userGame.save();

        res.redirect('/home');
    } catch (error) {
        res.render('SuperUser_Page/user_game/edit', {
            error: error
        });
    }
});

//API for delete user game
app.get("/home/api/delete_user_game/:id", async (req, res) => {
    try {
        const count = await user_game.destroy({
            where: { id: req.params.id }
        });

        if (count > 0) {
            return res.redirect('/home');
        } else {
            return res.redirect('/home');
        }
    } catch (err) {
        return res.redirect('/home');
    }

});
//API for edit user biodata
app.post("/home/api/edit_user_game/edit_biodata", urlEncoded, async (req, res) => {
    try {
        let userGameB = await user_game_biodatum.findOrCreate({
            where: {userId: req.params.id}
        });
        userGameB.firstName = req.body.firstName ? req.body.firstName : userGameB.firstName;
        userGameB.lastName = req.body.lastName ? req.body.lastName : userGameB.lastName;
        userGameB.birthDate = req.body.birthDate ? req.body.birthDate : userGameB.birthDate;
        await userGameB.save();

        res.redirect(`/home/show/${req.body.userId}/biodata`);
    } catch (error) {
        res.redirect('/home');
    }
});

//Api for create user biodata 
app.post("/home/api/create_user_biodata", urlEncoded, async (req, res) => {
    try {
        let userBiodata = await user_game_biodatum.create({
            userId: parseInt(req.body.userId),
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            birthDate: req.body.birthDate
        });

        res.redirect('/home');
    } catch (error) {
        res.render('SuperUser_Page/user_game/create', {
            error : error
        })
    }
});
app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`);
});
